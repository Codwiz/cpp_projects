#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Car {
private:
  int height;
  float speed;

public:
  Car() {}

  Car(int height, float speed) {
    this->height = height;
    this->speed = speed;

    print();
  }

  void setValues(int height, float speed) {
    this->height = height;
    this->speed = speed;
  }

  void print() { cout << this->height << " - " << this->speed << endl; }
};

class bike : public Car {};

int main() {

  bike bmw;
  bmw.setValues(600, 250);
  bmw.print();

  bike alpha;
  alpha.setValues(900, 380);
  alpha.print();

  Car shkoda(1100, 250);

  Car audi;
  audi.setValues(2000, 380);
  audi.print();

  return 0;
}